## What does this MR do?

<!-- Briefly describe what this MR is about -->

## Related issues

<!-- Link related issues below. -->

## Adding new filed or renaming existing one

1. [ ] I have considered if this field should be pseudonymized and updated [allowlist at snowplow pseudonymizer project](https://gitlab.com/gitlab-org/growth/product-intelligence/snowplow-pseudonymization/-/blob/e2d2a2b9188fb04aad91eeb8a3c686a399b96aee/lib/snowplow/gitlab_standard_context.rb#L9)

/label ~"product intelligence" ~"product intelligence::review pending" 
